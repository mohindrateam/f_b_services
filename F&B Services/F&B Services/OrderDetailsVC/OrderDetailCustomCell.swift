//
//  OrderDetailCustomCell.swift
//  F&B Services
//
//  Created by octal-mac on 20/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class OrderDetailCustomCell: UITableViewCell {
    
    // IBoutlets
    
    @IBOutlet weak var lblStatic:UILabel!
    @IBOutlet weak var lblDetail:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
