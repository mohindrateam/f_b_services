//
//  OrderDetailsVC.swift
//  F&B Services
//
//  Created by octal-mac on 20/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class OrderDetailsVC: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var tblRequestDetail:UITableView!
    @IBOutlet weak var btnTrackDriver:CustomButton!
    
    var arrStatic = ["Order Id","Contact Number","Estimated time to deliver","Estimated Fair","Time Duration"]
    
    var arrDynamic = ["#1234567890","123456789","2.3 hr","$120 - $200","30 Min. Approx"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()

           }
    
    // MARK:- Back Button Action
    @IBAction func backTapped(sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // Track Button Action
    @IBAction func driverTapped(sender:UIButton){
        
        self.navigationController?.pushViewController(TrackDriverVC(), animated: true)
    }

    // Set View
    func configure() {
        imgUser.layer.cornerRadius = imgUser.frame.width/2
        imgUser.layer.borderColor = UIColor.gray.cgColor
        imgUser.layer.borderWidth = 0.5
        
        tblRequestDetail.register(UINib.init(nibName: "OrderDetailCustomCell", bundle: nil), forCellReuseIdentifier: CustomCell.OrderDetailCell.rawValue)
        
        tblRequestDetail.estimatedRowHeight = 44.0
        
        tblRequestDetail.tableFooterView = UIView.init(frame: CGRect.zero)
        
        
    }
    
    

}

// MARK:- Table View Methods

extension OrderDetailsVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStatic.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblRequestDetail.dequeueReusableCell(withIdentifier: CustomCell.OrderDetailCell.rawValue) as! OrderDetailCustomCell
        cell.lblStatic.text = arrStatic[indexPath.row]
        cell.lblDetail.text = arrDynamic[indexPath.row]
        return cell
    }
    
    
}

extension OrderDetailsVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
