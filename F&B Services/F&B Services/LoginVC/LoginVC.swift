//
//  LoginVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 22/08/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import SVProgressHUD


class LoginVC: UIViewController {
    
    //***** Outlets ****//
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    
    //*** iVARS ***//
    let webService = Webservices()
    

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    
    //MAKR:- **** Actions ****//
    @IBAction func btn_Login_tapped(_ sender: CustomButton)
    {
        var params = NSMutableDictionary()
        params = ["mobile_number":txtMobileNumber.text!,
                      "password":txtPassword.text!,
                      "device_token":Constants.appDelegate.DeviceToken,
                      "device_os":DeviceInfo.DEVICE_PHONE_MODEL]
        
        print(params)
        
        webCall.webCalling(Parameter: params, header: Header.Login.rawValue, inVC: self){(response, status, message) in
            
            if status == true {
                if let data = response as? [String:Any]{
                    self.readUserInfo(userDic: data)
                    global.updateRootController()
                }
            }
            
        }
        
    }
    
    @IBAction func btn_SignUp_Tapped(_ sender: UIButton)
    {
        self.navigationController!.pushViewController(SignUpVC() as UIViewController, animated: true)
    }
    
    @IBAction func btn_ForgotPassword_Tapped(_ sender: UIButton)
    {
        
    }
    
    @IBAction func bnt_RememberMe_Tapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    func readUserInfo(userDic:[String:Any])
    {
        let userId = userDic["id"] as? String ?? ""
        let token = userDic["auth_token"] as? String ?? ""
        let name = userDic["username"] as? String ?? ""
        let email = userDic[""] as? String ?? ""
        let number = userDic["contact"] as? String ?? ""
        
        objInfo.initWithUserInfo(userid: userId, authtoken: token, fullname: name, emailid: email, mobileno: number)
        
        // Saving data to userdefault
        let data = NSKeyedArchiver.archivedData(withRootObject: objInfo)
        global.setUserDefault(data as AnyObject, KeyToSave: NSUserDefaultConstants.KEY_USER_DETAILS_NSDEFAULT)
    }
}

    
