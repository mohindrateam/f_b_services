//
//  SignUpVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 22/08/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func btn_SignIn_Tapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_AddAddress_Tapped(_ sender: Any)
    {
        self.navigationController?.pushViewController(AddAddressVC(), animated: true)
    }
}
