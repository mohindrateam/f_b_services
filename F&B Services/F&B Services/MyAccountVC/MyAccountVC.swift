//
//  MyAccountVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 19/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class MyAccountVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblAddress: UITableView!
    @IBOutlet weak var ConstAddressTableHeight: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        ConstAddressTableHeight.constant = tblAddress.contentSize.height
    }
    
    @IBAction func btn_Back_Tapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableView DataSource & Delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib.init(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
}
