//
//  UserInfo.swift
//  F&B Services
//
//  Created by octal-mac on 18/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class UserInfo: NSObject,NSCoding {
    
    var userID:String?
    var authToken:String?
    var fullName:String?
    var emailID:String?
    var mobileNo:String?
    
    required init(ID:String, auth:String, name:String, email:String, mobile:String ) {
        self.userID = ID
        self.authToken = auth
        self.fullName = name
        self.emailID = email
        self.mobileNo = mobile
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.userID = aDecoder.decodeObject(forKey: "userID") as? String ?? ""
        self.authToken = aDecoder.decodeObject(forKey: "authToken") as? String ?? ""
        self.fullName = aDecoder.decodeObject(forKey: "fullName") as? String ?? ""
        self.emailID = aDecoder.decodeObject(forKey: "emailID") as? String ?? ""
        self.mobileNo = aDecoder.decodeObject(forKey: "mobileNo") as? String ?? ""
    }
    
    func encode(with aCoder:NSCoder){
        aCoder.encode(userID, forKey: "userID")
        aCoder.encode(authToken, forKey: "authToken")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(emailID, forKey: "emailID")
        aCoder.encode(mobileNo, forKey: "mobileNo")
    }
    
    func initWithUserInfo(userid:String, authtoken:String, fullname:String, emailid:String, mobileno:String){
        self.userID = userid
        self.authToken = authtoken
        self.fullName = fullname
        self.emailID = emailid
        self.mobileNo = mobileno
    }
    

}
