//
//  Global.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 22/08/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class Global: NSObject
{
    
    // MARK:- Logout function
    func logoutUser() {
        removeUserDefault(NSUserDefaultConstants.KEY_USER_DETAILS_NSDEFAULT)
        objInfo = UserInfo.init(ID: "", auth: "", name: "", email: "", mobile: "")
        setRootViewController()

    }
    // MARK: - Get Current User Info
    func getCurrentUserInfo(){
        let loginUserDetails : Data?
        loginUserDetails = getUserDefault(NSUserDefaultConstants.KEY_USER_DETAILS_NSDEFAULT) as? Data
        
        if loginUserDetails != nil {
            let userInfo : UserInfo = NSKeyedUnarchiver.unarchiveObject(with: loginUserDetails!) as! UserInfo
            
            objInfo.initWithUserInfo(userid: userInfo.userID!, authtoken: userInfo.authToken!, fullname: userInfo.fullName!, emailid: userInfo.emailID!, mobileno: userInfo.mobileNo!)
        }
    }
    
    // MARK:- Set Root View Controller
    func setRootViewController() {
        getCurrentUserInfo()
        if objInfo.userID == "" {
        let login:LoginVC?
        Constants.appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        login = LoginVC(nibName: "LoginVC", bundle: nil);
        Constants.appDelegate.navController = UINavigationController(rootViewController: login!);
        Constants.appDelegate.navController?.isNavigationBarHidden = true
        Constants.appDelegate.window?.rootViewController = Constants.appDelegate.navController
        Constants.appDelegate.window?.makeKeyAndVisible()
        }
        else
        {
            updateRootController()
        }
    }
    
    // MARK:- Update Root View Controller
    func updateRootController()
    {
        let slide:SWRevealViewController = SWRevealViewController.init(rearViewController: SideMenuVC(), frontViewController: HomeVC())
        Constants.appDelegate.navController = UINavigationController.init(rootViewController: slide)
        Constants.appDelegate.window?.rootViewController = Constants.appDelegate.navController
        Constants.appDelegate.window?.makeKeyAndVisible()

    }
    
    func setNavigationSliderMenuButton (_ objRevealVC: SWRevealViewController, menuButton btnMenu: UIButton, currentView view: UIView)
    {
        btnMenu.addTarget(objRevealVC, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
        //objRevealVC.rightViewRevealWidth = 200
        objRevealVC.rearViewRevealWidth =   UIScreen.main.bounds.size.width
        view.addGestureRecognizer(objRevealVC.panGestureRecognizer())
        //Customize Slide Menu
        customizeSlideMenu(objRevealVC)
    }
    
    func customizeSlideMenu (_ objRevealVC: SWRevealViewController)
    {
        objRevealVC.rearViewRevealDisplacement = 60.0
        // how much of the menu is shown (default 260.0)
        // objRevealVC.rightViewRevealWidth = 280
        objRevealVC.rearViewRevealWidth =   UIScreen.main.bounds.size.width/2
        // TOGGLING ANIMATION: Configure the animation while the menu gets hidden
        objRevealVC.rearViewRevealOverdraw = 0.0
        // Animation type (SWRevealToggleAnimationTypeEaseOut or SWRevealToggleAnimationTypeSpring)
        objRevealVC.toggleAnimationType = SWRevealToggleAnimationType.easeOut
        // Duration for the revealToggle animation (default 0.25)
        objRevealVC.toggleAnimationDuration = 0.50
        // damping ratio if SWRevealToggleAnimationTypeSpring (default 1.0)
        objRevealVC.springDampingRatio = 1.0
        
        // SHADOW: Configure the shadow that appears between the menu and content views
        // radius of the front view's shadow (default 2.5)
        objRevealVC.frontViewShadowRadius = 10.0
        // radius of the front view's shadow offset (default {0.0f,2.5f})
        objRevealVC.frontViewShadowOffset = CGSize(width: 0.0, height: 2.5)
        // front view's shadow opacity (default 1.0)
        objRevealVC.frontViewShadowOpacity = 0.8
        // front view's shadow color (default blackColor)
        objRevealVC.frontViewShadowColor = UIColor.init(netHex: 0x334393)
    }


    //MARK: - Save/Get NSUserDefaults
    func setUserDefault(_ ObjectToSave : AnyObject?  , KeyToSave : String)
    {
        if (ObjectToSave != nil)
        {
            Constants.defaults.set(ObjectToSave, forKey: KeyToSave)
        }
        
        UserDefaults.standard.synchronize()
    }
    
    func getUserDefault(_ KeyToReturnValue : String) -> AnyObject?
    {
        if let name = Constants.defaults.value(forKey: KeyToReturnValue)
        {
            return name as AnyObject?
        }
        return nil
    }
    
    func removeUserDefault(_ KeyToReturnValue : String)
    {
        Constants.defaults.removeObject(forKey: KeyToReturnValue)
    }

}



//**** MARK :- CustomTextfield ****//
@IBDesignable
class CustomTextField: UITextField,UITextFieldDelegate
{
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        delegate = self
        
    }
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
        delegate = self
        
    }
    
    //**** OUTLES ****//
    @IBInspectable var borderColor: UIColor = UIColor.clear
    @IBInspectable var ActiveBorderColor: UIColor = UIColor.clear
    @IBInspectable var InactiveBorderColor: UIColor = UIColor.clear
    @IBInspectable var placeholderColor: UIColor = UIColor.black
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var leftSpace: CGFloat = 0.0
    @IBInspectable var bottomLine: Bool = false
    @IBInspectable var bottomBorderColor: UIColor = UIColor.clear
    
    override func awakeFromNib()
    {

        setBorderColor(borderColor: borderColor)
        setBorderWidth(borderWidth: borderWidth)
        setCornerRadius(cornerRadius: cornerRadius)
        setLeftSpace(leftSpace: leftSpace)
        setPlaceHolderColor(placeholderColor: placeholderColor)
        setBottomLine(bottomLine: bottomLine)
    }
    
    func setBorderColor(borderColor:UIColor)
    {
        self.layer.borderColor = borderColor.cgColor
    }
    
    func setBorderWidth(borderWidth:CGFloat)
    {
        self.layer.borderWidth = borderWidth
    }
    
    func setCornerRadius(cornerRadius:CGFloat)
    {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
    
    func setPlaceHolderColor(placeholderColor:UIColor)
    {
      
        self.attributedPlaceholder = NSAttributedString.init(string: self.placeholder ?? "", attributes:[NSForegroundColorAttributeName:placeholderColor])
       
    }
    
    func setLeftSpace(leftSpace:CGFloat)
    {
        self.leftView = UIView.init(frame:CGRect.init(x: 0.0, y: 0.0, width: leftSpace, height: self.frame.size.height))
        self.leftViewMode = .always
    }
    
    func setBottomLine(bottomLine:Bool)
    {
        if(bottomLine)
        {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = bottomBorderColor.cgColor
        border.frame = CGRect(x: 0, y: self.frame.height-width, width:  self.frame.size.width + 50, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        }
    }
    
    //**** Mark:- UITextFieldDelegate ****//
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.bottomBorderColor = ActiveBorderColor
        setBottomLine(bottomLine: bottomLine)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.bottomBorderColor = InactiveBorderColor
        setBottomLine(bottomLine: bottomLine)
    }

        
}

//**** MARK :- CustomTextfield ****//
@IBDesignable
class CustomButton: UIButton
{
    //**** OUTLETS ****//
    @IBInspectable var borderColor: UIColor = UIColor.red
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var enableShadow: Bool = false
    
    override func awakeFromNib()
    {
        setBorderColor(borderColor: borderColor)
        setBorderWidth(borderWidth: borderWidth)
        setCornerRadius(cornerRadius: cornerRadius)
        setShadow(enable: enableShadow)
    }
    
    func setBorderColor(borderColor:UIColor)
    {
        self.layer.borderColor = borderColor.cgColor
    }
    
    func setBorderWidth(borderWidth:CGFloat)
    {
        self.layer.borderWidth = borderWidth
        
    }
    
    func setCornerRadius(cornerRadius:CGFloat)
    {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
    
    func setShadow(enable:Bool)
    {
        if enable
        {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 6
        self.layer.shadowOffset = CGSize.init(width:-2.0, height:5.0)
        }
    }
}

//**** MARK :- CustomTextfield ****//
@IBDesignable
class CustomView: UIView
{
    //**** OUTLETS ****//
    @IBInspectable var borderColor: UIColor = UIColor.red
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var cornerRadius: CGFloat = 0.0
    

    override func awakeFromNib()
    {
        setBorderColor(borderColor: borderColor)
        setBorderWidth(borderWidth: borderWidth)
        setCornerRadius(cornerRadius: cornerRadius)
    }
    
    func setBorderColor(borderColor:UIColor)
    {
        self.layer.borderColor = borderColor.cgColor
    }
    
    func setBorderWidth(borderWidth:CGFloat)
    {
        self.layer.borderWidth = borderWidth
    }
    
    func setCornerRadius(cornerRadius:CGFloat)
    {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}


//**** MARK :- CustomImageView ****//
@IBDesignable
class CustomImageView: UIImageView
{
    //**** OUTLETS ****//
    @IBInspectable var borderColor: UIColor = UIColor.red
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var cornerRadius: CGFloat = 0.0
    
    
    override func awakeFromNib()
    {
        setBorderColor(borderColor: borderColor)
        setBorderWidth(borderWidth: borderWidth)
        setCornerRadius(cornerRadius: cornerRadius)
    }
    
    func setBorderColor(borderColor:UIColor)
    {
        self.layer.borderColor = borderColor.cgColor
    }
    
    func setBorderWidth(borderWidth:CGFloat)
    {
        self.layer.borderWidth = borderWidth
    }
    
    func setCornerRadius(cornerRadius:CGFloat)
    {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}




