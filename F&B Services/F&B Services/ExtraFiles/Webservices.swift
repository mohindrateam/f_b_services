//
//  Webservices.swift
//  Swift3Demo
//
//  Created by Octal on 01/02/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class Webservices: NSObject {

    func webCalling(Parameter: NSDictionary, header:String, inVC: UIViewController, closure: @escaping (_ responce : AnyObject?, _ SuccessBool: Bool, _ message:String) -> ()) {
        
        SVProgressHUD .show()
        Alamofire.request("\(AppURL.BASE_URL)\(header)", method: .post, parameters:Parameter as? Parameters).responseJSON
            {
                (response:DataResponse<Any>) in
                
                SVProgressHUD.dismiss()
                print(response.request ?? "")
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        print(response.result.value!)
                        let JSON = response.result.value! as AnyObject
                        let responce = JSON as! NSDictionary
                        let sucess = responce.value(forKey: "response_code") as? Int
                        let msg = responce.value(forKey: "message") as? String ?? ""
                        if sucess == 200 {
                        let data = responce.value(forKey: "data") as? NSDictionary
                        closure(data, true, msg)
                        }

                    else{
                        print("Fails")
                        closure(nil, false, msg)
                        self.presentAlertWithTitle(title: "App Name", message: msg, vc: inVC)
                    }
                }
                    break
                        
                    
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    closure(nil, false, (response.result.error?.localizedDescription)!)
                    self.presentAlertWithTitle(title: "App Name", message: (response.result.error?.localizedDescription)!, vc: inVC)
                    break
                }
        }
        
    }
    
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        alertController.view.tintColor = UIColor.init(netHex: 0x27C59A)
        vc.present(alertController, animated: true, completion: nil)
    }
 
    

}
