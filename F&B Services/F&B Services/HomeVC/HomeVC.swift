//
//  HomeVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 25/08/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

class HomeVC: UIViewController {

    @IBOutlet weak var viewGoogleMap: GMSMapView!
    @IBOutlet weak var btnRequestNow: UIButton!
    @IBOutlet weak var btnScheduleRequest: UIButton!
    @IBOutlet weak var lblOR: CustomButton!
    @IBOutlet weak var btnMenu:UIButton!
  
    //******** Outlets for View Finding Driver ********//
    @IBOutlet weak var viewFindingDriver: UIView!
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var lblDestinationAddress: UILabel!
    @IBOutlet weak var viewShowTimerAnimation: UIView!
    @IBOutlet weak var lblTimerCountDown: UILabel!
    
    
    var marker = GMSMarker()
    var marker2 = GMSMarker()
    let circle = CAShapeLayer()
    var time = 60
    var timer = Timer()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupGoogleMap()
        
        viewFindingDriver.isHidden = true
        
        if self.revealViewController() != nil
        {
            global.setNavigationSliderMenuButton(revealViewController(), menuButton: btnMenu, currentView: view)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        lblTimerCountDown.isHidden = true
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.drawCircleWithAnimation),
            name: NSNotification.Name("DrawCircleNotification"),
            object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewDidLayoutSubviews() {
        
    }
    
    
    
    
    @IBAction func btnMenuTapped(sender:UIButton){
        
        
    }
    
    @IBAction func btn_PickupLocation_Tapped(_ sender: Any)
    {
        self.navigationController?.pushViewController(AllRequestsVC(), animated: true)
    }
    
    
    @IBAction func btn_Destination(_ sender: Any)
    {
        self.navigationController?.pushViewController(MyAccountVC(), animated: true)
    }
    
    @IBAction func btn_RequestNow_Tapped(_ sender: UIButton)
    {
        self.navigationController?.pushViewController(RequestNowVC(), animated: true)
    }
    
    @IBAction func btn_Schedule_Tapped(_ sender: UIButton)
    {
        self.navigationController?.pushViewController(ScheduleRequestVC() as UIViewController, animated: true)
    }
    
    
    @IBAction func btn_CancelOngoingRequest_Tapped(_ sender: CustomButton)
    {
      //  timer.invalidate()
        time = 60
        viewFindingDriver.isHidden = true
        circle.removeFromSuperlayer()
        lblTimerCountDown.isHidden = true
    }
    
    
    // **** Helper Methods ***//
    
    func setupGoogleMap()
    {
        viewGoogleMap.addSubview(btnRequestNow)
        viewGoogleMap.addSubview(btnScheduleRequest)
        viewGoogleMap.addSubview(lblOR)
        
        let camera = GMSCameraPosition.camera(withLatitude: 29.373483, longitude: 47.974700,
                                              zoom: 16)
        viewGoogleMap.camera = camera
        marker = GMSMarker.init(position: CLLocationCoordinate2DMake(29.373483, 47.974700))
        marker2 = GMSMarker.init(position: CLLocationCoordinate2DMake(29.367129, 47.974273))
        marker.icon = #imageLiteral(resourceName: "PinDrivers")
        marker2.icon = #imageLiteral(resourceName: "PinDrivers")
        marker.map = viewGoogleMap
        marker2.map = viewGoogleMap
//        self.drawPath()
    }
    
    func drawCircleWithAnimation()
    {
        
        time = 60
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(startTimer), userInfo: nil, repeats: true)
        
        viewFindingDriver.isHidden = false
        let radius = 100
        circle.path = UIBezierPath.init(roundedRect: CGRect.init(x: 0, y: 0, width: 2*radius, height: 2*radius), cornerRadius: CGFloat(radius)).cgPath
        
        circle.position = CGPoint.init(x: (self.viewShowTimerAnimation.frame.width/2) - CGFloat(radius), y: (self.viewShowTimerAnimation.frame.height/2) - CGFloat(radius))
        
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor.init(netHex: 0x27C59A).cgColor
        circle.lineWidth = 5.0
        
        self.viewShowTimerAnimation.layer.addSublayer(circle)
        
        let drawAnimation :CABasicAnimation = CABasicAnimation.init(keyPath: "strokeEnd")
        
        drawAnimation.duration = 60.0
        drawAnimation.repeatCount = 1.0
        
        drawAnimation.fromValue = 1.0
        drawAnimation.toValue = 0.0
        
        drawAnimation.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionLinear)
        
        circle.add(drawAnimation, forKey: "drawCircleAnimation")
    }
    
    func startTimer()
    {
        if time == 1
        {
            time = 60
            viewFindingDriver.isHidden = true
            circle.removeFromSuperlayer()
            
        }
        else
        {
            lblTimerCountDown.isHidden = false
            time -= 1
            print(time)
            lblTimerCountDown.text = "\(time) Seconds left"
        }
        
    }
    
    
    func drawPath()
    {
        let origin =  "\(marker.position.latitude),\(marker.position.longitude)"
        let destination = "\(marker2.position.latitude),\(marker2.position.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=walking&key=AIzaSyBye0P47xar9-ULsehyaPjARfLLxSPMkC8"
        
        Alamofire.request(url).responseJSON { response in
            print(response.result)
            
            do {
                let json = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any]
                let routes = json?["routes"] as! NSArray
                
                for route in routes
                {
                    let obj : NSDictionary = (route as? NSDictionary)!
                    let routeOverviewPolyline = obj["overview_polyline"] as? NSDictionary
                    let points = routeOverviewPolyline?.value(forKey: "points") as? String
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 7.0
                    polyline.map = self.viewGoogleMap
                }
            }
            catch{}
        }

    }
}
