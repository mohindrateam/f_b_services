//
//  RequestNowVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 15/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class RequestNowVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var viewPickerBackground: UIView!
    @IBOutlet var viewSelectPaymentMode: UIView!
    @IBOutlet weak var pkrSelectPaymentMode: UIPickerView!
    @IBOutlet weak var lblPickerSelectedValue: UILabel!
    @IBOutlet weak var txtMobile: CustomTextField!
    @IBOutlet weak var txtPayment: CustomTextField!
    @IBOutlet weak var txtDeliveryCharges: CustomTextField!
    @IBOutlet weak var txtPaymentMode: CustomTextField!
    @IBOutlet weak var btnSelectPaymentMode: UIButton!
    
    
    let arrPaymetMode = ["Cash","Credit Card","Prepaid"]
   
   //*********** Implementation **********//
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        pkrSelectPaymentMode.selectedRow(inComponent: 0)
        viewPickerBackground.isHidden = true
        viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        viewPickerBackground.addSubview(viewSelectPaymentMode)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func btn_ContinueToRequest_Tapped(_ sender: CustomButton)
    {
        NotificationCenter.default.post(name: NSNotification.Name("DrawCircleNotification"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Back_Tapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SelectPaymentMode_Tapped(_ sender: Any)
    {
        txtPaymentMode.borderColor = UIColor.init(red: 39/255.0, green: 197/255.0, blue: 154/255.0, alpha: 1.0)
        
        UIView.animate(withDuration: 0.5)
        {
            self.viewPickerBackground.isHidden = false
            self.viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height-self.viewSelectPaymentMode.frame.size.height, width: UIScreen.main.bounds.width, height: 300)
        }
    }
    
    
    //MARK:- UIPickerView Methodes 
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arrPaymetMode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrPaymetMode[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("\(row)")
        lblPickerSelectedValue.text = arrPaymetMode[row]
    }
    
    @IBAction func btn_PickerCancel_Tapped(_ sender: Any)
    {
        
        UIView.animate(withDuration: 0.5)
        {
            self.viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        }
        viewPickerBackground.isHidden = true
        txtPaymentMode.borderColor = .lightGray
        
    }
    @IBAction func btn_PickerConfirm_Tapped(_ sender: Any)
    {
        txtPaymentMode.text = lblPickerSelectedValue.text
        UIView.animate(withDuration: 0.5)
        {
            self.viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        }
        viewPickerBackground.isHidden = true
        txtPaymentMode.borderColor = .lightGray
    }
    

    
    
}
