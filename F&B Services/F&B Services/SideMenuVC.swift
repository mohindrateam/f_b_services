//
//  SideMenuVC.swift
//  F&B Services
//
//  Created by octal-mac on 19/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var tblMenu:UITableView!
    
    var arrSideMenu = ["My Account","My Orders","Payments","Help","Logout"]
    var navigation = UINavigationController()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        lblUserName.text = objInfo.fullName
    }
    
    // MARK:- Configure 
    func configure() {
        // User Image Border
        imgUser.layer.borderColor = UIColor.white.cgColor
        imgUser.layer.borderWidth = 2.0
        imgUser.layer.cornerRadius = imgUser.frame.width/2
        
        tblMenu.register(UINib.init(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: CustomCell.SideCell.rawValue)
        tblMenu.tableFooterView = UIView.init(frame: CGRect.zero)

        navigation.viewControllers.append(HomeVC())
        navigation.isNavigationBarHidden = true
        
    }


}

extension SideMenuVC:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMenu.dequeueReusableCell(withIdentifier: CustomCell.SideCell.rawValue) as! SideMenuCell
        cell.lblSideMenu.text = arrSideMenu[indexPath.row]
        return cell
    }
    
}

extension SideMenuVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            // My Account Navigation
        case 0:
            if (!navigation.viewControllers.contains(MyAccountVC())){
                navigation.viewControllers.append(MyAccountVC())
            }else{
                navigation.popToViewController(MyAccountVC(), animated: true)
            }
          break
            // My Orders Navigation
        case 1:
            if (!navigation.viewControllers.contains(AllRequestsVC())){
                navigation.viewControllers.append(AllRequestsVC())
            }else{
                navigation.popToViewController(AllRequestsVC(), animated: true)
            }
            break
           // Payments Navigation
        case 2:
            if (!navigation.viewControllers.contains(PaymentsVC())){
                navigation.viewControllers.append(PaymentsVC())
            }else{
                navigation.popToViewController(PaymentsVC(), animated: true)
            }
            break
          // Help Navigation
        case 3:
            if (!navigation.viewControllers.contains(HelpVC())){
                navigation.viewControllers.append(HelpVC())
            }else{
                navigation.popToViewController(HelpVC(), animated: true)
            }
            break
          // Logout
        case 4:
            global.logoutUser()
            break
        default:
            break
        }
        
        revealViewController().pushFrontViewController(navigation, animated: true)
    }
}
