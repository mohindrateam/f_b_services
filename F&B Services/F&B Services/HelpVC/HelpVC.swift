//
//  HelpVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 20/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import MessageUI
import Messages

class HelpVC: UIViewController,MFMessageComposeViewControllerDelegate {


    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btn_Back_Tapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Call Function
    func callNumber(phoneNumber:String)
    {
        
        if let url = URL(string: "telprompt://\(phoneNumber)")
        {
            if UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // MARK:- SMS Functions
    func message(number:[String],message:String?)
    {
        if MFMessageComposeViewController.canSendText() == true
        {
            let messageController = MFMessageComposeViewController()
            messageController.messageComposeDelegate = self
            messageController.recipients = number
            messageController.body = message ?? ""
            self.present(messageController, animated: true, completion: nil)
        }
        else
        {
            //handle text messaging not available
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        
    }


}
