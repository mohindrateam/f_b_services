//
//  ScheduleRequestVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 15/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class ScheduleRequestVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    @IBOutlet weak var viewPickerBackground: UIView!
    @IBOutlet var viewSelectDateTime: UIView!
    @IBOutlet var viewSelectPaymentMode: UIView!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var txtPayment: CustomTextField!
    @IBOutlet weak var txtDeliveryCharges: CustomTextField!
    @IBOutlet weak var txtDateOfDelivery: CustomTextField!
    @IBOutlet weak var txtTimeOfDelivery: CustomTextField!
    @IBOutlet weak var txtPaymentMode: CustomTextField!
    @IBOutlet weak var lblSelectedDateTime: UILabel!
    @IBOutlet weak var lblSelectedDateTimeValve: UILabel!
    @IBOutlet weak var pkrDateTime: UIDatePicker!
    @IBOutlet weak var pkrPaymentMode: UIPickerView!
    @IBOutlet weak var lblSelectedPaymentMode: UILabel!
    
    let arrPaymetMode = ["Cash","Credit Card","Prepaid"]
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        pkrPaymentMode.selectedRow(inComponent: 0)
        viewPickerBackground.isHidden = true
        viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        viewSelectDateTime.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        viewPickerBackground.addSubview(viewSelectPaymentMode)
        viewPickerBackground.addSubview(viewSelectDateTime)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btn_Back_Tapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SelectDate_Tapped(_ sender: Any)
    {
        pkrDateTime.tag = 1001
        pkrDateTime.datePickerMode = .date
        pkrDateTime.minimumDate = Date()
        let formater = DateFormatter()
        formater.dateFormat = "dd/MM/yyyy"
        lblSelectedDateTimeValve.text = formater.string(from: Date())
        lblSelectedDateTime.text = "Selected Date"
        UIView.animate(withDuration: 0.5)
        {
            self.viewPickerBackground.isHidden = false
            self.viewSelectDateTime.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height-self.viewSelectPaymentMode.frame.size.height, width: UIScreen.main.bounds.width, height: 300)
        }
    }
    
    @IBAction func btn_SelectTime_Tapped(_ sender: Any)
    {
        pkrDateTime.tag = 1002
        pkrDateTime.datePickerMode = .time
        let calendar = Calendar.current
        var hour = calendar.component(.hour, from: Date())
        hour += 3
        
        pkrDateTime.minimumDate = Date()
        let formater = DateFormatter()
        formater.dateFormat = "hh:mm a"
        lblSelectedDateTimeValve.text = formater.string(from: Date())
        lblSelectedDateTime.text = "Selected Time"
        UIView.animate(withDuration: 0.5)
        {
            self.viewPickerBackground.isHidden = false
            self.viewSelectDateTime.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height-self.viewSelectPaymentMode.frame.size.height, width: UIScreen.main.bounds.width, height: 300)
        }
    }
    
    @IBAction func btn_SelectPaymentMode_Tapped(_ sender: Any)
    {
        UIView.animate(withDuration: 0.5)
        {
            self.viewPickerBackground.isHidden = false
            self.viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height-self.viewSelectPaymentMode.frame.size.height, width: UIScreen.main.bounds.width, height: 300)
        }
    }
    
    //MARK:- UIPickerView Methodes
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arrPaymetMode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrPaymetMode[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("\(row)")
        lblSelectedPaymentMode.text = arrPaymetMode[row]
    }
    
    @IBAction func DatePickerChanged(_ sender: UIDatePicker)
    {
        switch sender.tag {
        case 1001:// For Selected Date
            let formater = DateFormatter()
            formater.dateFormat = "dd/MM/yyyy"
            lblSelectedDateTimeValve.text = formater.string(from: sender.date)
            break
        case 1002:// For Selected Time
            let formater = DateFormatter()
            formater.dateFormat = "hh:mm a"
            lblSelectedDateTimeValve.text = formater.string(from: sender.date)
            break
        default:
            break
        }
        
    }
  
    @IBAction func btn_PaymentModePickerCancel_Tapped(_ sender: Any)
    {
        
        UIView.animate(withDuration: 0.5)
        {
            self.viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        }
        viewPickerBackground.isHidden = true
        
    }
    @IBAction func btn_PaymentModePickerConfirm_Tapped(_ sender: Any)
    {
        txtPaymentMode.text = lblSelectedPaymentMode.text
        UIView.animate(withDuration: 0.5)
        {
            self.viewSelectPaymentMode.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        }
        viewPickerBackground.isHidden = true
    }
    
    @IBAction func btn_DateTimePickerCancel_Tapped(_ sender: Any)
    {
        
        UIView.animate(withDuration: 0.5)
        {
            self.viewSelectDateTime.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        }
        viewPickerBackground.isHidden = true
        
    }
    @IBAction func btn_DateTimePickerConfirm_Tapped(_ sender: Any)
    {
        switch pkrDateTime.tag {
        case 1001:// For Selected Date
            txtDateOfDelivery.text = lblSelectedDateTimeValve.text
            break
        case 1002:// For Selected Time
            txtTimeOfDelivery.text = lblSelectedDateTimeValve.text
            break
        default:
            break
        }
        UIView.animate(withDuration: 0.5)
        {
            self.viewSelectDateTime.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: 300)
        }
        viewPickerBackground.isHidden = true
    }
}
